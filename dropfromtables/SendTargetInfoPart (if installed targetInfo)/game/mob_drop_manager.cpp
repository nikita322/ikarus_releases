//search at the end of the file (should be last line)
#endif



/*
	ADD BEFOREEEEEEEEEEEEEE  WARING BEFOREEEEEEEEEEEEEEEE
*/

#ifdef __SEND_TARGET_INFO__
void MOB_DROP_MANAGER::MakeDropInfoItems(LPCHARACTER pkChar, LPCHARACTER pkKiller, std::vector<LPITEM>& items)
{

	if(!pkChar || !pkKiller)
		return;

	LPITEM item					= NULL;
	ITEM_MANAGER& rItemManager	= ITEM_MANAGER::instance();
	DWORD dwMobVnum				= pkChar->GetRaceNum();

	//common drop
	{
		BYTE bRank = pkChar->GetMobRank();
		COMMON_DROP_MAP::iterator it = m_commonDropMap.find(bRank);

		if (it != m_commonDropMap.end())
		{
			COMMON_DROP_VEC& rVec = it->second;
			COMMON_DROP_VEC::iterator itVec = rVec.begin();

			for ( ; itVec != rVec.end() ; itVec++)
			{
				CommonDropInfo& rDropInfo = *itVec;

				if (rDropInfo.iLevelStart > pkKiller->GetLevel())
					continue;

				if (rDropInfo.iLevelEnd < pkKiller->GetLevel())
					continue;

				TItemTable * table = rItemManager.GetTable(rDropInfo.dwItemVnum);

				if (!table)
					continue;

				item = NULL;
				item = rItemManager.CreateItem(rDropInfo.dwItemVnum, 1, 0, true);

				if (item)
				{
					if (table->bType == ITEM_POLYMORPH && rDropInfo.dwItemVnum == pkChar->GetPolymorphItemVnum())
						item->SetSocket(0, pkChar->GetRaceNum());

					items.push_back(item);
				}
			}
		}
	}





	//default drop
	{
		DEFAULT_DROP_MAP::iterator it = m_defaultDropMap.find(dwMobVnum);

		if (it != m_defaultDropMap.end())
		{
			DEFAULT_DROP_VEC& rVec = it->second;
			DEFAULT_DROP_VEC::iterator itVec = rVec.begin();

			for (; itVec != rVec.end(); itVec++)
			{
				DefaultDropInfo& rDropInfo = *itVec;


				item = NULL;
				item = rItemManager.CreateItem(rDropInfo.dwItemVnum, rDropInfo.iCount, 0, true);

				if (item->GetType() == ITEM_POLYMORPH && item->GetVnum() == pkChar->GetPolymorphItemVnum())
					item->SetSocket(0, dwMobVnum);

				items.push_back(item);
			}
		}
	}



	//mob drop kill
	{
		MOB_DROP_GROUP_KILL_MAP_VNUM::iterator it = m_dropGroupKillMap.find(dwMobVnum);

		if (it != m_dropGroupKillMap.end())
		{
			MOB_DROP_GROUP_KILL_MAP_ID& rGroupMap = it->second;
			MOB_DROP_GROUP_KILL_MAP_ID::iterator itGroup = rGroupMap.begin();

			for (; itGroup != rGroupMap.end(); itGroup++)
			{
				MobDropKillGroupInfo& rGroupInfo = itGroup->second;

				if(rGroupInfo.IsEmpty())
					continue;

				for (size_t i = 0; i < rGroupInfo.itemVector.size(); i++)
				{
					MobDropKillInfo& rDropInfo = rGroupInfo.itemVector[i];
					item = NULL;
					item = rItemManager.CreateItem(rDropInfo.dwItemVnum, rDropInfo.iCount, 0, true);

					if (item)
					{
						if (item->GetType() == ITEM_POLYMORPH && item->GetVnum() == pkChar->GetPolymorphItemVnum())
							item->SetSocket(0, dwMobVnum);

						items.push_back(item);
					}
				}
			}
		}
	}




	//mob drop level
	{
		MOB_DROP_GROUP_LEVEL_MAP_VNUM::iterator it = m_dropGroupLevelMap.find(dwMobVnum);

		if (it != m_dropGroupLevelMap.end())
		{
			MOB_DROP_LEVEL_ID_MAP&			rMapID	= it->second;
			MOB_DROP_LEVEL_ID_MAP::iterator	itID	= rMapID.begin();

			for (; itID!=rMapID.end(); itID++)
			{
				MobDropLevelGroupInfo& rGroupInfo = itID->second;
				if(pkKiller->GetLevel() < rGroupInfo.iLevelStart)
					continue;

				if(pkKiller->GetLevel() > rGroupInfo.iLevelEnd)
					continue;

				MOB_DROP_LEVEL_INFO_VEC& rDropVec = rGroupInfo.itemVector;
				MOB_DROP_LEVEL_INFO_VEC::iterator itDrop = rDropVec.begin();

				for(; itDrop!=rDropVec.end();itDrop++)
				{
					MobDropLevelInfo& rDropInfo = *itDrop;
					item = NULL;
					item = rItemManager.CreateItem(rDropInfo.dwItemVnum,rDropInfo.iCount, 0, true);
					if (item)
					{
						if (item->GetType() == ITEM_POLYMORPH && item->GetVnum() == pkChar->GetPolymorphItemVnum())
							item->SetSocket(0, dwMobVnum);

						items.push_back(item);
					}
				}
			}
		}
	}


}
#endif
