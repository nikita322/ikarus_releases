	PLAYER_EXP_TABLE_MAX	= 120,
	PLAYER_MAX_LEVEL_CONST	= 250,

	GUILD_MAX_LEVEL			= 20,
	MOB_MAX_LEVEL			= 100,
#ifdef ENABLE_DROP_FROM_TABLE
	//working with 1000/1000 in drop Pct (1 = 1 chance on 1000 times) (the real chance depend on different between mob's level and killer's level)
	DROP_TABLE_SCALE		= 1000, 
#endif
	ATTRIBUTE_MAX_VALUE		= 20,
	CHARACTER_PATH_MAX_NUM	= 64,
	SKILL_MAX_NUM			= 255,
	SKILLBOOK_DELAY_MIN		= 64800,