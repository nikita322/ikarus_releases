#ifndef __INCLUDE_HEADER_TEXTLINE_SDTGUI__
#define __INCLUDE_HEADER_TEXTLINE_SDTGUI__

namespace sdtgui
{
	void makeDefaultFont(TFontInfo& rFont);


	class TEXTLINE : public WINDOW
	{
	public:
		TEXTLINE();
		~TEXTLINE();

		bool				onRender();

		void				draw(HDC& hdc);
		void				clear();

		void				setText(std::wstring text);
		void				setText(std::string  text);
		void				drawText(HDC& hdc);

		std::wstring		getTextAsWString();
		std::string			getTextAsString();
		void				getText(char* szBuff, DWORD dwMaxLen);

		void				setFont(TFontInfo* fontInfo);
		void				getFont(TFontInfo& r_out);

		COLORREF			getColor();
		void				setColor(COLORREF rColor);

		void				applyHorizontalCenterAling(bool isActive);

		void				setOnChangeTextEvent(TWidgetEvent& event);
		void				onChangeText();

	private:
		std::wstring		m_wsText;
		TFontInfo			m_fontInfo;
		COLORREF			m_color;
		UINT				m_uiFormat;

		TWidgetEvent*		m_onChangeText;
	};
}

#endif __INCLUDE_HEADER_TEXTLINE_SDTGUI__