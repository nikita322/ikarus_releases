#ifndef __INCLUDE_HEADER_DRAW_SDTGUI__
#define __INCLUDE_HEADER_DRAW_SDTGUI__
namespace sdtgui
{
	void DrawStaticBitmap(HBITMAP hbitmap, HDC& hdc, long cx, long cy, long w, long h, long offx, long offy, bool srcSize);
	void DrawRoudRect(HDC& hdc, long left, long top, long right, long bottom, long w, long h, COLORREF color );
	void DrawDynamicBitmap(HBITMAP hbitmap, HDC& hdc, long cx, long cy, long w, long h, TRectScale& rectScale);
}
#endif //__INCLUDE_HEADER_DRAW_SDTGUI__
