#ifndef __INCLUDE_HEADER_GUI_SDTGUI__
#define __INCLUDE_HEADER_GUI_SDTGUI__

namespace sdtgui
{

	class GUI
	{

	public:
		typedef  std::list<WINDOW*> ROOTLIST;
		typedef  std::map<UINT,TMessageEvent> MESSAGEMAP;
		typedef  std::map<std::string,LAYER> LAYERMAP;

	public:
		GUI();
		~GUI();

		void		clear();

		bool		onRender();
		void		render(HDC& hdc);

		bool		messageProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		void		registerMessageEvent(UINT type, TMessageEvent& event);

		LAYER*		addLayer(std::string layername);
		LAYER*		getLayer(std::string layername);
		bool		removeLayer(std::string layername);
		
		WINDOW*		getPicked(TCoordinate x , TCoordinate y);
		WINDOW*		getPicked(TPos pos);





	private:
		MESSAGEMAP	m_messageEventMap;
		LAYERMAP	m_layerMap;
		TPos		m_mousePos;

	};
}

#endif //__INCLUDE_HEADER_GUI_SDTGUI__