#ifndef __INCLUDE_HEADER_IMAGE_SDTGUI__
#define __INCLUDE_HEADER_IMAGE_SDTGUI__	

namespace sdtgui
{
	class IMAGE : public WINDOW
	{
	public:
		IMAGE();
		~IMAGE();

		void		clear();
		bool		onRender();

		void		draw(HDC& hdc);

		//the set size operation reset the rectscale, so warning to use it before to set the scale if you need it
		void		setSize(TCoordinate width, TCoordinate height);

		//if the size of the IMAGE obj is 0 , this methods set the size (Using WINDOW::setSize) getting it from the HBITMAP
		void		setImage(HBITMAP newImage);

		//this is to repeat the image (or to truncate it for value < 1.0f)
		void		setRectScale(float rLeft, float rTop, float rRight, float rBottom);
		void		setRectScale(TRectScale& rScale);

		TRectScale	getRectScale();
		void		getRectScale(TRectScale& rScale);
		void		getRectScale(float& rLeft, float& rTop, float& rRight, float& rBottom);


	private:
		TRectScale		m_rectScale;

		//this is used to repeat/truncate the image (the WINDOW m_size is used instead to take the real size of the image)
		TSize			m_imageSize;
		HBITMAP			m_image;

	};
}

#endif //__INCLUDE_HEADER_IMAGE_SDTGUI__


