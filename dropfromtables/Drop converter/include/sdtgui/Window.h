#ifndef __HEADER_INCLUDE_WINDOWS_SDTGUI__
#define __HEADER_INCLUDE_WINDOWS_SDTGUI__


namespace sdtgui
{
	

	class WINDOW
	{
	public:
		typedef std::list<WINDOW*> CHILDREN;

	public:
		WINDOW();
		virtual ~WINDOW();
		virtual void		clear();
		virtual bool		onRender();
		virtual void		draw(HDC& hdc);
		virtual bool		isIn(TPos & pos);
		virtual bool		isIn(TCoordinate x, TCoordinate y);
		
		virtual void		setLayerLevel(TLayerLevel layer);
		virtual TLayerLevel getLayerLevel();

		virtual void		setWidgetType(TWidgetType type);
		virtual TWidgetType getWidgetType();

		virtual bool		isWidgetType(TWidgetType type);

		virtual void		setPosition(TPos& pos);
		virtual void		setPosition(TPos* pos);
		virtual void		setPosition(TCoordinate x, TCoordinate y);

		virtual void		setSize(TSize* size);
		virtual void		setSize(TSize& size);
		virtual void		setSize(TCoordinate w, TCoordinate h);

		virtual void		setParent(WINDOW* parent);
		virtual void		addChild(WINDOW* pchild);
		virtual void		removeChild(WINDOW* pChild);
		virtual bool		hasChild(WINDOW* pchild);

		virtual void		setTop();
		virtual void		setChildTop(WINDOW* child);

		virtual void		setOnMouseLeftButtonUpEvent(TWidgetEvent& event);
		virtual void		setOnMouseLeftButtonDownEvent(TWidgetEvent& event);
		virtual void		setOnMouseRightButtonUpEvent(TWidgetEvent& event);
		virtual void		setOnMouseRightButtonDownEvent(TWidgetEvent& event);
		virtual void		setOnRedrawEvent(TWidgetEvent& event);

		virtual void		setOnShowEvent(TWidgetEvent& event);
		virtual void		setOnHideEvent(TWidgetEvent& event);
		virtual void		setOnResizeEvent(TWidgetEvent& event);
		virtual void		setOnMoveEvent(TWidgetEvent& event);

		virtual void		onMouseLeftButtonUp();
		virtual void		onMouseLeftButtonDown();
		virtual void		onMouseRightButtonUp();
		virtual void		onMouseRightButtonDown();

		virtual void		onRedraw();

		virtual void		onHide();
		virtual void		onShow();
		virtual void		onResize();
		virtual void		onMove();


		virtual void		setOnMouseOverInEvent(TWidgetEvent& event);
		virtual void		setOnMouseOverOutEvent(TWidgetEvent& event);

		virtual void		onMouseOverIn();
		virtual void		onMouseOverOut();


		virtual void		setID(int iCommand);
		virtual int			getID();

		virtual TPos		getPosition();
		virtual void		getPosition(TPos& r_pos);
		virtual void		getPosition(TCoordinate& x, TCoordinate& y);

		virtual TPos		getGlobalPosition();
		virtual void		getGlobalPosition(TPos& r_pos);
		virtual void		getGlobalPosition(TCoordinate& x, TCoordinate& y);

		virtual void		callUpdate();
		virtual bool		isUpdate();

		virtual void		setFocus();
		virtual void		killFocus();

		WINDOW*				getChildren(int iID);
		void				getChildren(int iID, WINDOW** pWin);
		
		WINDOW*				getPicked(TPos& pos);

		virtual TCoordinate		getX();
		virtual TCoordinate		getY();

		virtual TCoordinate		getWidth();
		virtual TCoordinate		getHeight();

		virtual TSize			getSize();
		virtual void			getSize(TSize& r_size);
		virtual void			getSize(TCoordinate& w, TCoordinate& h);

		virtual void			getRect(TRect& rect);
		virtual TRect			getRect();
		virtual void			getRect(RECT& rect);


		virtual void			setWindowName(std::wstring name);
		virtual std::wstring	getWindowName();

		virtual void			show();
		virtual void			hide();

		virtual bool			isShown();

		virtual void			setBackground(TBackgroundInfo* info);

	public:
		//the usage of this metods is only for use in addChild, don't use it without set parent
		virtual void			setParentPointer(WINDOW* parent);

	private:
		TPos						m_pos;
		TSize						m_size;

		WINDOW*						m_parent;
		CHILDREN					m_childrenList;

		TLayerLevel					m_layerLevel;
		TWidgetType					m_widgetType;
		
		bool						m_bIsUpdate;
		bool						m_bIsFocused;

		TWidgetEvent*				m_onMouseLeftUpEvent;
		TWidgetEvent*				m_onMouseLeftDownEvent;
		TWidgetEvent*				m_onMouseRightUpEvent;
		TWidgetEvent*				m_onMouseRightDownEvent;

		TWidgetEvent*				m_onRedrawEvent;

		TWidgetEvent*				m_onMouseOverInEvent;
		TWidgetEvent*				m_onMouseOverOutEvent;

		TWidgetEvent*				m_onResizeEvent;
		TWidgetEvent*				m_onMoveEvent;
		TWidgetEvent*				m_onShowEvent;
		TWidgetEvent*				m_onHideEvent;

		TRenderEvent*				m_onRenderEvent;

		int							m_ID;
		std::wstring				m_wsName;
		bool						m_bIsShow;
		TBackgroundInfo*			m_bgInfo;

	};
}

#endif //__HEADER_INCLUDE_WINDOWS_SDTGUI__

