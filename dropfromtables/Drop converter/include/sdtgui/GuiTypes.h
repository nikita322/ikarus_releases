#ifndef __INCLUDE_GUI_TYPES_HEADER_SDTGUI__
#define __INCLUDE_GUI_TYPES_HEADER_SDTGUI__






namespace sdtgui
{

	typedef long	TLayerLevel;
	typedef short	TWidgetType;
	typedef int		TWidgetDictKey;
	typedef long	TCoordinate;

	typedef std::function<bool(int,void*)> TWidgetEvent;
	typedef std::function<void(void*)> TRenderEvent;

	typedef std::function<bool(HWND hWnd, UINT message,
		WPARAM wParam, LPARAM lParam)> TMessageEvent;


	typedef struct SPos
	{
		TCoordinate x;
		TCoordinate y;

		void assign(SPos& r)
		{
			x=r.x;
			y=r.y;
		}

		void assign(TCoordinate argx, TCoordinate argy)
		{
			x=argx;
			y=argy;
		}

		void operator +=(SPos& p)
		{
			x += p.x;
			y += p.y;
		}

	}TPos;

	typedef struct SSize
	{
		TCoordinate width;
		TCoordinate height;

		void assign(SSize& r)
		{
			width	=r.width;
			height	=r.height;
		}

		void assign(TCoordinate w, TCoordinate h)
		{
			width	= w;
			height	= h;
		}


		
	} TSize;

	typedef struct SRect
	{
		TCoordinate left;
		TCoordinate top;
		TCoordinate right;
		TCoordinate bottom;


		void assign(SRect& r)
		{
			left	= r.left;
			top		= r.top;
			right	= r.right;
			bottom	= r.bottom;
		}

		void assign(TPos& p , TSize& s)
		{
			left	= p.x;
			top		= p.y;

			right	= left + s.width;
			bottom	= top  + s.height;
		}


		void assign(TSize& s, TPos& p)
		{
			assign(p,s);
		}

		void operator()(TPos& p, TSize& s)
		{
			assign(p,s);
		}

		void operator()(TSize& s, TPos& p)
		{
			assign(p,s);
		}

		void toWindowsRect(RECT & rect)
		{
			rect.top	= top;
			rect.left	= left;
			rect.right	= right;
			rect.bottom	= bottom;
		}

	} TRect;


	//textline

	typedef struct SFontInfo
	{
		int    cHeight;
		int    cWidth;
		int    cEscapement;
		int    cOrientation;
		int    cWeight;
		DWORD  bItalic;
		DWORD  bUnderline;
		DWORD  bStrikeOut;
		DWORD  iCharSet;
		DWORD  iOutPrecision;
		DWORD  iClipPrecision;
		DWORD  iQuality;
		DWORD  iPitchAndFamily;
		LPCWSTR pszFaceName;

	} TFontInfo;



	//buttons

	typedef struct SButtonCoverInfo
	{
		HBITMAP defaultCover;
		HBITMAP overCover;
		HBITMAP downCover;

		COLORREF colorIgnore; 

	} TButtonCoverInfo;


	typedef struct SButtonColorInfo
	{
		COLORREF defaultColor;
		COLORREF overColor;
		COLORREF downColor;
	} TButtonColorInfo;


	//for image
	typedef struct SRectScale
	{
		float fTopScale, fLeftScale, fBottomScale, fRightScale;

		void getRealSize(TSize& size)
		{
			TCoordinate horizontalLeft	= (TCoordinate) -(size.width* (fLeftScale-1.0f));
			TCoordinate horizontalRight = (TCoordinate) (size.width * fRightScale);

			TCoordinate horizontalBegin = SDT_MIN(horizontalLeft, horizontalRight);
			TCoordinate horizontalEnd	= SDT_MAX(horizontalLeft, horizontalRight);

			TCoordinate verticalTop		= (TCoordinate) -(size.height * (fTopScale - 1.0f));
			TCoordinate verticalBottom	= (TCoordinate) (size.height * (fBottomScale));

			TCoordinate verticalBegin	= SDT_MIN(verticalTop, verticalBottom);
			TCoordinate verticalEnd		= SDT_MAX(verticalTop, verticalBottom);


			size.assign(horizontalEnd - horizontalBegin , verticalEnd - verticalBegin);
		}
	} TRectScale;




	typedef struct SBackgroundInfo
	{
		COLORREF bg_color;
		COLORREF border_color;
	} TBackgroundInfo;



	//GUI types
	
}



#endif //__INCLUDE_GUI_TYPES_HEADER_SDTGUI__