#ifndef __INCLUDE_HEADER_BUTTON_SDTGUI__
#define __INCLUDE_HEADER_BUTTON_SDTGUI__

namespace sdtgui
{
	class BUTTON : public WINDOW
	{
	public:
		typedef UINT8 TButtonStatus;

	public:
		BUTTON();
		~BUTTON();

		void	clear();
		bool	onRender();

		void	draw(HDC& hdc);


		void			setText(std::wstring text);
		void			setText(std::string text);
		void			setText(const char* text, DWORD dwLen);


		std::wstring	getTextAsWString();
		std::string		getTextAsString();
		void			getText(char* szBuff, DWORD dwLen);


		void			setCover(TButtonCoverInfo* cover);
		void			getCover(TButtonCoverInfo* out);


		void			setColors(TButtonColorInfo* pinfo);
		void			getColors(TButtonColorInfo* pinfo);
		void			setTextColor(COLORREF color);

		void			setOnClickEvent(TWidgetEvent& event);

		bool			isOver();
		bool			isUp();
		bool			isDown();

		void		onMouseLeftButtonUp();
		void		onMouseLeftButtonDown();

		void		onMouseOverIn();
		void		onMouseOverOut();

		void		setSize(TSize* size);
		void		setSize(TSize& size);
		void		setSize(TCoordinate w, TCoordinate h);

		void		drawCover(HDC& hdc);
		void		drawText(HDC& hdc);

		bool		isCovered();


	private:
		TButtonCoverInfo*		m_coverInfo;
		TButtonColorInfo		m_colors;
		COLORREF				m_textColor;
		
		TWidgetEvent*			m_onClickEvent;
		TButtonStatus			m_buttonStatus;
		TEXTLINE*				m_textline;
	};
}







#endif //__INCLUDE_HEADER_BUTTON_SDTGUI__

