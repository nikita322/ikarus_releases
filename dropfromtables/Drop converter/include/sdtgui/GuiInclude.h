#ifndef __INCLUDE_GUI_INCLUDE_HEADER__SDTGUI__
#define __INCLUDE_GUI_INCLUDE_HEADER__SDTGUI__


#define WIN32_LEAN_AND_MEAN             // Escludere gli elementi usati raramente dalle intestazioni di Windows

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif //_CRT_SECURE_NO_WARNINGS


#include <Windows.h>

//stl
#include <map>
#include <vector>
#include <list>
#include <functional>


#define SDT_ZERO_MEMORY(pointer,size) memset(pointer,0,size) 
#define SDT_ZERO_OBJECT(obj) memset(&obj, 0 , sizeof(obj))


//widget event
#define SDT_WEVENT_FUNC_TYPE bool(int,void*)
#define SDT_WEVENT_LAMBDA_TYPE [](int p, void* c)

#define SDT_WIDGET_EVENT(name) bool name(int command, void* pArg)
#define SDT_DECLARE_WIDGET_EVENT_FROM_LAMBDA(name,expression) std::function<SDT_FUNC_TYPE> name = expression
#define SDT_EMPTY_WIDGET_EVENT SDT_WEVENT_LAMBDA_TYPE{ return true; }
#define SDT_WIDGET_LAMBDA(expression) SDT_WEVENT_LAMBDA_TYPE expression

//render event
#define SDT_RENDER_EVENT(name) void name (HDC& hdc, void*)
#define SDT_EMPTY_RENDER_EVENT [](HDC& hdc, void* win){}

//casting derivated pointer to the parent pointer (WINDOW*)
#define SDT_TO_WINDOW_POINTER(pObj) (dynamic_cast<WINDOW*>(pObj))

//stuff to create/delete element in heap
//this define works fine to change it in few seconds for the use in the source
#define SDT_NEW(something) new something
#define SDT_DELETE(s) delete(s)
#define SDT_DELETE_ARRAY(s) delete[](s)


//message dispatch
#define SDT_MESSAGE_EVENT(funcname) bool funcname(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)




//general define
#define SDT_MAX(v1,v2) (v1 > v2	?	v1:v2 )
#define SDT_MIN(v1,v2) (v1 > v2	?	v2:v1 )
#define SDT_MODULE(v1) (v1 > 0	?	v1:-v1)

#define SDT_TOCOORDINATE(val)		(static_cast<TCoordinate>(val))
#define SDT_TOFLOAT(val)			(static_cast<float>(val))

#include "GuiConstants.h"
#include "GuiTypes.h"



#include "Draw.h"

//widget
#include "Window.h"
#include "Textline.h"
#include "Button.h"
#include "Image.h"

#include "Layer.h"
#include "Gui.h"

#ifdef _DEBUG
#pragma comment(lib , "SDTGuiLib(MTd).lib")
#else
#pragma comment(lib , "SDTGuiLib(MT).lib")
#endif

#endif //__INCLUDE_GUI_INCLUDE_HEADER__SDTGUI__