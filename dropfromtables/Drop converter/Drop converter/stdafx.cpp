#include "stdafx.h"



void logFunc(const char* fmt, ...)
{
	static char szLine[1000];
	memset(szLine, 0 , sizeof(szLine));

	va_list arg;
	va_start(arg, fmt);
	vsnprintf(szLine, sizeof(szLine), fmt,  arg);
	va_end(arg);

	printf("%s\n", szLine);
}

void wlogFunc(const wchar_t* fmt, ...)
{
	static wchar_t szLine[1000];
	memset(szLine, 0 , sizeof(szLine));

	va_list arg;
	va_start(arg, fmt);
	wvsprintfW(szLine, fmt,  arg);
	va_end(arg);

	wprintf(L"%s\n", szLine);
}