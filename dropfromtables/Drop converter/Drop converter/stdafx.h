// stdafx.h: file di inclusione per file di inclusione del sistema standard
// o file di inclusione specifici del progetto usati di frequente, ma
// modificati raramente
//

#pragma once

#include "targetver.h"

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS 1
#endif


#define WIN32_LEAN_AND_MEAN             // Escludere gli elementi usati raramente dalle intestazioni di Windows
// File di intestazione di Windows
#include <windows.h>
#include <commdlg.h>

// File di intestazione Runtime C
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <mutex>
#include <thread>
#include <atomic>
#include <fstream>
#include <string>
#include <stdarg.h>

//including global features from msl & sdtgui
#include <msl/msl.h>
#include <sdtgui/GuiInclude.h>

//pragma as library sdtguilibrary
#ifdef _DEBUG
#pragma comment(lib , "SDTGuiLib(MTd).lib")
#else
#pragma comment(lib , "SDTGuiLib(MT).lib")
#endif

void logFunc(const char* fmt, ...);
void wlogFunc(const wchar_t* fmt, ...);


#ifdef _DEBUG
#	define CONSOLE(fmt, ...)	logFunc(fmt, __VA_ARGS__)
#	define WCONSOLE(s,...)		wlogFunc(s, __VA_ARGS__)
#else
#	define CONSOLE(...) 
#	define WCONSOLE(...)
#endif

// fare riferimento qui alle intestazioni aggiuntive richieste dal programma
