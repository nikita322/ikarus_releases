/*
 Navicat Premium Data Transfer
 Date: 09/05/2019 20:54:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for drop_common
-- ----------------------------
DROP TABLE IF EXISTS `drop_common`;
CREATE TABLE `drop_common`  (
  `rank` smallint(255) UNSIGNED NULL DEFAULT 0,
  `item_vnum` bigint(11) UNSIGNED NULL DEFAULT 0,
  `level_start` int(11) UNSIGNED NULL DEFAULT 0,
  `level_end` int(11) UNSIGNED NULL DEFAULT 1000,
  `prob` bigint(11) UNSIGNED NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for drop_default
-- ----------------------------
DROP TABLE IF EXISTS `drop_default`;
CREATE TABLE `drop_default`  (
  `mob_vnum` bigint(11) UNSIGNED NULL DEFAULT 0,
  `item_vnum` bigint(11) UNSIGNED NULL DEFAULT 0,
  `count` int(11) UNSIGNED NULL DEFAULT 0,
  `prob` int(11) UNSIGNED NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for drop_mob_group_kill
-- ----------------------------
DROP TABLE IF EXISTS `drop_mob_group_kill`;
CREATE TABLE `drop_mob_group_kill`  (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `mob_vnum` bigint(11) UNSIGNED NULL DEFAULT 0,
  `kill_per_drop` int(11) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for drop_mob_group_level
-- ----------------------------
DROP TABLE IF EXISTS `drop_mob_group_level`;
CREATE TABLE `drop_mob_group_level`  (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `mob_vnum` bigint(11) UNSIGNED NOT NULL DEFAULT 0,
  `level_start` bigint(11) UNSIGNED NOT NULL DEFAULT 0,
  `level_end` bigint(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for drop_mob_item_kill
-- ----------------------------
DROP TABLE IF EXISTS `drop_mob_item_kill`;
CREATE TABLE `drop_mob_item_kill`  (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `item_vnum` bigint(11) UNSIGNED NOT NULL,
  `count` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `part_prob` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for drop_mob_item_level
-- ----------------------------
DROP TABLE IF EXISTS `drop_mob_item_level`;
CREATE TABLE `drop_mob_item_level`  (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `item_vnum` bigint(11) UNSIGNED NOT NULL DEFAULT 0,
  `count` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `prob` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
