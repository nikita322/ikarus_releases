#pragma once

typedef std::wstring WSTRING;
typedef std::vector<std::wstring> DIRECTORIES;
typedef std::list<std::wstring> FILES;


DWORD GetCRC32(const char* buffer, size_t count);
DWORD GetCaseCRC32(const char * buf, size_t len);
DWORD GetHFILECRC32(HANDLE hFile);
DWORD GetFileCRC32(const wchar_t* c_szFileName);
DWORD GetFileSize(const wchar_t* c_szFileName);

bool GetFilesFromDir(WSTRING dir,FILES& vec , WSTRING format);
bool GetDirectoriesFromDir(WSTRING dir,DIRECTORIES& vec, bool isRecursive=true);
